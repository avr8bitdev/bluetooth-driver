/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Bluetooth_functions.h"
#include "../../MCAL_drivers/USART_driver/USART_functions.h"
#include "../../MCAL_drivers/INT_driver/INT_functions.h"
#include <util/delay.h>


// --- internal --- //
// receiving buffer
#define BT_RxBuff_LENGTH 48
static u8 BT_RxBuff[BT_RxBuff_LENGTH] = {0};

// helper flags
static volatile u8 isRxComplete = 0;    // is CMD received fully ?
static volatile u8 isLastActionCMD = 0; // was last action a command ? (indicator)

// CMD pin place-holders
static DIO_port_t BTportCMD;
static u8 BTpinCMD;

// callback for when a byte (data) is received
BT_CB_t BT_CB_byte = 0;

// UART Rx complete callback:
// This is called when BT device receives a byte (from outside)
// or sends a response to a command that was issued earlier.
// The function stores the CMD response in a buffer and sends anything
// else to the callback function (if registered).
static void BT_vidRxByte(u16 u16ByteCpy)
{
	static u8 i = 0;  // indexer for receiving buffer

	// if (last action was not a CMD), then data is assumed
	if (!isLastActionCMD)
	{
		if (BT_CB_byte) // if a CB is registered, call it and pass the byte
			BT_CB_byte( (u8)u16ByteCpy );

		return;
	}

	isRxComplete = 0; // reset Rx buffer-state on each byte received,
	                  // this logically makes sense because the flag
	                  // should only be set to 1 when '\n' is received

	BT_RxBuff[i++] = (u8)u16ByteCpy; // store byte in buffer, then inc indexer

	// if (buffer end reached) or (termination sequence received)
	// then this is considered a termination signal
	if (    (i >= BT_RxBuff_LENGTH)
		 || ( (u16ByteCpy == '\n') && (BT_RxBuff[i-2] == '\r') )
	   )
	{
		i = 0;               // reset indexer
		isLastActionCMD = 0; // reset CMD indicator, this logically makes sense
		                     // because next byte may be from outside (data),
		                     // this should be set to 1 by whoever issued the CMD,
		                     // else the received byte is assumed to be data
		                     // and not a CMD response.
		isRxComplete = 1;    // state that Rx buffer received a complete string/CMD
	}
}

static inline void BT_vidPollRxFlag(void)
{
	while (!isRxComplete)
	{}

	isRxComplete = 0; // reset flag
}

static inline u8 BT_u8isRxCMD_OK(void)
{
    if (    (BT_RxBuff[0] == 'O')
         && (BT_RxBuff[1] == 'K')
       )
        return 1;

    return 0;
}
// ---------------- //

u8 BT_u8init(DIO_port_t enumPortCMDCpy, const u8 u8PinCMDCpy, const char* charPtrNameCpy, const char* charPtrPINCpy)
{
	// CMD pin
    if ((BTportCMD != PORT_UNUSED) && (BTpinCMD != PIN_UNUSED))
    {
        DIO_vidSet_pinDirection(enumPortCMDCpy, u8PinCMDCpy, OUTPUT);
        DIO_vidSet_pinValue(enumPortCMDCpy, u8PinCMDCpy, OFF);
    }

    // update CMD pin place-holders
    BTportCMD = enumPortCMDCpy;
    BTpinCMD  = u8PinCMDCpy;

	// init USART
    USART_vidInit( USART_data_size_8bits,
    		       USART_parity_disabled,
				   USART_stopBit_1bit,
				   USART_Baud_rate_16MHz_38400,
				   USART_asynch
				 );

    // register receiving function
    USART_vidRegisterCB_RxComplete(BT_vidRxByte, 0);

    // enable interrupts
    USART_vidEnableRxCompleteINT();

    INT_vidEnable_global_flag();

    // power on delay, ensures first CMD would be sent correctly
    // undocumented, by trial and error.
    _delay_ms(1000);

    // test USART connection
    // this is just to waste the first transmission signal
    // since it's sometimes wrong
    BT_u8TestUART();

    // test USART connection again
    register u8 res = BT_u8TestUART();

    BT_vidSetRole(BT_role_slave);

    BT_vidChangeName(charPtrNameCpy);

    BT_vidChangePIN(charPtrPINCpy);

    return res;
}

void BT_vidSendCMD(const char* charPtrCMDCpy)
{
	// set CMD indicator
	isLastActionCMD = 1;

	// select CMD mode
	if ((BTportCMD != PORT_UNUSED) && (BTpinCMD != PIN_UNUSED))
	    DIO_vidSet_pinValue(BTportCMD, BTpinCMD, ON);

	// send 'AT+'
	USART_vidTransmitStr("AT+");

	// send CMD
	USART_vidTransmitStr(charPtrCMDCpy);

	// terminate with <CR><LF>
	USART_vidTransmitStr("\r\n");

    // restore Data mode
    if ((BTportCMD != PORT_UNUSED) && (BTpinCMD != PIN_UNUSED))
        DIO_vidSet_pinValue(BTportCMD, BTpinCMD, OFF);

    BT_vidPollRxFlag();
}

void BT_vidSendChar(const u8 u8DataCpy, u8 u8isTerminateCpy)
{
    // select Data mode
    if ((BTportCMD != PORT_UNUSED) && (BTpinCMD != PIN_UNUSED))
        DIO_vidSet_pinValue(BTportCMD, BTpinCMD, OFF);

    // send Data
    USART_vidTransmitData(u8DataCpy);

    if (u8isTerminateCpy)
        USART_vidTransmitStr("\r\n"); // terminate with <CR><LF>
}

void BT_vidSendStr(const char* charPtrDataCpy, u8 u8isTerminateCpy)
{
    // select Data mode
    if ((BTportCMD != PORT_UNUSED) && (BTpinCMD != PIN_UNUSED))
        DIO_vidSet_pinValue(BTportCMD, BTpinCMD, OFF);

    // send Data
    USART_vidTransmitStr(charPtrDataCpy);

    if (u8isTerminateCpy)
        USART_vidTransmitStr("\r\n"); // terminate with <CR><LF>
}

u8 BT_u8TestUART(void)
{
    // set CMD indicator
    isLastActionCMD = 1;

    // select CMD mode
    if ((BTportCMD != PORT_UNUSED) && (BTpinCMD != PIN_UNUSED))
        DIO_vidSet_pinValue(BTportCMD, BTpinCMD, ON);

    // send 'AT'
    USART_vidTransmitStr("AT\r\n");

    // restore Data mode
    if ((BTportCMD != PORT_UNUSED) && (BTpinCMD != PIN_UNUSED))
        DIO_vidSet_pinValue(BTportCMD, BTpinCMD, OFF);

    BT_vidPollRxFlag();

    return BT_u8isRxCMD_OK();
}

inline void BT_vidRegisterCB(BT_CB_t funcPtrCpy)
{
	BT_CB_byte = funcPtrCpy;
}

inline void BT_vidDeregisterCB(void)
{
    BT_CB_byte = 0;
}

inline void BT_vidReset(void)
{
	BT_vidSendCMD("RESET");

	// reset needs additional delay, since the BT device
	// stops responding to commands for some time after reset.
	_delay_ms(2000);
}

void BT_vidSetRole(BT_role_t enumRoleCpy)
{
    switch (enumRoleCpy)
    {
        case BT_role_slave:
            BT_vidSendCMD("ROLE=0");
        break;

        case BT_role_master:
            BT_vidSendCMD("ROLE=1");
        break;
    }
}

void BT_vidChangeName(const char* charPtrNameCpy)
{
	u8 i;
	char temp_buff[31 + 5 + 1];
	temp_buff[0] = 'N';
	temp_buff[1] = 'A';
	temp_buff[2] = 'M';
	temp_buff[3] = 'E';
	temp_buff[4] = '=';
	for (i = 0; (i < 31) && charPtrNameCpy[i]; i++)
	{
		if ((charPtrNameCpy[i] != ' ') && (charPtrNameCpy[i] != '.'))
		    temp_buff[i+5] = charPtrNameCpy[i];
		else
			temp_buff[i+5] = '_';
	}

	temp_buff[i+5] = 0;

	BT_vidSendCMD(temp_buff);
}

void BT_vidChangePIN(const char charPtrPINCpy[4])
{
    char temp_buff[16 + 5 + 1];
    u8 i = 0;
    temp_buff[0] = 'P';
    temp_buff[1] = 'S';
    temp_buff[2] = 'W';
    temp_buff[3] = 'D';
    temp_buff[4] = '=';
    for (i = 0; (i < 16) && charPtrPINCpy[i]; i++)
    {
        temp_buff[i+5] = charPtrPINCpy[i];
    }

    temp_buff[i+5] = 0;

    BT_vidSendCMD(temp_buff);
}

