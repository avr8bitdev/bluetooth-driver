/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HAL_DRIVERS_BLUETOOTH_DRIVER_BLUETOOTH_FUNCTIONS_H_
#define HAL_DRIVERS_BLUETOOTH_DRIVER_BLUETOOTH_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"
#include "../../MCAL_drivers/DIO_driver/DIO_functions.h"

typedef enum
{
    BT_role_slave,
    BT_role_master
} BT_role_t;

typedef void (*BT_CB_t)(u8); // callback for when a byte is received

#define PORT_UNUSED 0xFF
#define PIN_UNUSED  0xFF

u8 BT_u8init(DIO_port_t enumPortCMDCpy, const u8 u8PinCMDCpy, const char* charPtrNameCpy, const char* charPtrPINCpy);

void BT_vidSendCMD(const char* charPtrCMDCpy);
void BT_vidSendChar(const u8 u8DataCpy, u8 u8isTerminateCpy);
void BT_vidSendStr(const char* charPtrDataCpy, u8 u8isTerminateCpy);

u8 BT_u8TestUART(void);

void BT_vidRegisterCB(BT_CB_t funcPtrCpy);
void BT_vidDeregisterCB(void);

void BT_vidReset(void);

void BT_vidSetRole(BT_role_t enumRoleCpy);

void BT_vidChangeName(const char* charPtrNameCpy);
void BT_vidChangePIN(const char charPtrPINCpy[4]);


#endif /* HAL_DRIVERS_BLUETOOTH_DRIVER_BLUETOOTH_FUNCTIONS_H_ */

